package com.gmail.vladgural;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

@SaveTo(filePass = "file.txt")
public class Contaner {
	private String text = "Save me";
	
	@Saver
	public void save(File file) {
		try(PrintWriter pw = new PrintWriter(file)){
			System.out.println("Writing to file");
			pw.print(text);
		}catch (FileNotFoundException e) {
			System.out.println("File not found Exceptin");	
		}
	}
}
