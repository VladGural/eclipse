package com.gmail.vladgural;

import java.io.File;
import java.lang.reflect.*;

public class SaverClass {
	public static void saveClass(Object obj) {
		Class<?> cls = obj.getClass();
		
		if(!cls.isAnnotationPresent(SaveTo.class))
			return;
		SaveTo saveTo = cls.getAnnotation(SaveTo.class);
		String filePass = saveTo.filePass();
		File file = new File(filePass);
		
		Method[] methods = cls.getMethods();
		for(Method method:methods) {
			if(method.isAnnotationPresent(Saver.class)) {
				try {
					method.invoke(obj, file);
				}catch (Exception e) {}	
			}
		}
	}
}
