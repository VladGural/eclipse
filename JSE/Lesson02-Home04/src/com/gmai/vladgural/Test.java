package com.gmai.vladgural;

public final class Test {
	public static String p = "Test string";
	private int[][] array = {{12,15},{12,12}};
	private String[] str = {"A","B"};
	private int a = 7;
	protected long b = 8;
	
	
	public Test() {}
	public Test(int a) { this.a = a; }
	public Test(int a, long b) { this.a = a; this.b = b; }
	public int getA() { return a; }
	public long getB() { return b; }
	public void setA(int a) { this.a = a; }
}