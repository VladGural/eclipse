package com.gmai.vladgural;

import java.lang.reflect.*;

public class ClassInfo {
	
	
	public String modifiersToString(int mods) {
		String modsString = "";
		
		if(Modifier.isPrivate(mods))
			modsString += "private ";
		if(Modifier.isProtected(mods))
			modsString += "protected ";
		if(Modifier.isPublic(mods))
			modsString += "public ";
		if(Modifier.isStatic(mods))
			modsString += "static ";
		if(Modifier.isAbstract(mods))
			modsString += "abstract ";
		if(Modifier.isFinal(mods))
			modsString += "final ";
		if(Modifier.isTransient(mods))
			modsString += "transient ";
		if(Modifier.isVolatile(mods))
			modsString += "volatile ";
		if(Modifier.isSynchronized(mods))
			modsString += "synchronized ";
		if(Modifier.isNative(mods))
			modsString += "native ";
		
		return modsString;
	}
	
	public String getTypeName(String fieldTypeName){
		
		if(fieldTypeName.charAt(0)=='[') {
			int i;
			
			for(i=0; i<fieldTypeName.length(); i++) {
				if(fieldTypeName.charAt(i)!='[')
					break;
			}
			
			switch(fieldTypeName.charAt(i)) {
			case 'Z': fieldTypeName = "bolean";
						break;
			case 'B': fieldTypeName = "byte";
						break;
			case 'C': fieldTypeName = "char";
						break;
			case 'D': fieldTypeName = "double";
						break;
			case 'F': fieldTypeName = "float";
						break;			
			case 'I': fieldTypeName = "int";
						break;
			case 'J': fieldTypeName = "long";
						break;
			case 'S': fieldTypeName = "short";
						break;			
			case 'L': fieldTypeName = fieldTypeName.substring(i+1, fieldTypeName.length()-1); 			
						
			}
			for(int j=0; j<i; j++)
				fieldTypeName += "[]";
			
		}
		return fieldTypeName;
	}
	
	public void getClassInfo(Object obj) {
		Class<?> cls =  obj.getClass();
		int mods;
		
		//Look at a class
		String stringClass = "";
		mods=cls.getModifiers();
		Class<?> sClass = cls.getSuperclass();
		Class<?>[] inter = cls.getInterfaces();

		stringClass += modifiersToString(mods) + "class " + cls.getName();
		if(sClass!=null){
			stringClass += " extends " + sClass.getName();
		}	
		if(inter.length!=0) {
			stringClass += " implements ";
			for(Class<?> cl:inter){
				stringClass += cl.getName() + ", ";
			}
		}
		
		stringClass = stringClass.substring(0, stringClass.length()-2);
		
		stringClass += " {";
		System.out.println(stringClass);
		System.out.println();
		
		
		//Look at fields of class
		System.out.println("//Fields of class");
		Field[] fields = cls.getDeclaredFields();
		for(Field field:fields) {
			mods=field.getModifiers();
			Class<?> fieldType = field.getType();
			String fieldTypeName = fieldType.getName();
			fieldTypeName=getTypeName(fieldTypeName);
			
 			System.out.print("\t" + modifiersToString(mods) + fieldTypeName + " " + field.getName() + ";");
			System.out.println();
		}
		System.out.println();
		
		//Look at Constructors of class
		System.out.println("//Constructors of class");
		String strConstructor;
		Constructor<?>[] constructors = cls.getDeclaredConstructors();
		for(Constructor<?> constructor:constructors) {
			strConstructor = "\t";
			mods=constructor.getModifiers();
			strConstructor += modifiersToString(mods);
			strConstructor += constructor.getName();
			strConstructor +="(";
			Class<?>[] parametersType = constructor.getParameterTypes();
			for(Class<?> parameterType:parametersType) {
				strConstructor += getTypeName(parameterType.getName()) + ", ";
			}
			if(parametersType.length > 0)
				strConstructor = strConstructor.substring(0, strConstructor.length()-2);
			strConstructor +=");";
			
			System.out.println(strConstructor);
		}
		System.out.println();
		
		
		//Look at methods of class
		System.out.println("//Methods of class");
		String strMethod = "";
		Method[] methods = cls.getDeclaredMethods();
		Class<?> returnType = null;
		for(Method method:methods){
			strMethod ="\t";
			mods=method.getModifiers();
			strMethod += modifiersToString(mods);
			returnType = method.getReturnType();
			strMethod += getTypeName(returnType.getName());
			strMethod += " " + method.getName() + "(";
			Class<?>[] parametersType = method.getParameterTypes();
			for(Class<?> parameterType:parametersType) {
				strMethod += getTypeName(parameterType.getName()) + ", ";
			}
			if(parametersType.length > 0)
				strMethod = strMethod.substring(0, strMethod.length()-2);
			
			strMethod +=")";
			
			
			Class<?>[] exceptionsType = method.getExceptionTypes();
			if(exceptionsType.length!=0)
				strMethod += " throws ";
			for(Class<?> exceptionType:exceptionsType) {
				strMethod += exceptionType.getName() + ", ";
			}
			if(exceptionsType.length > 0)
				strMethod = strMethod.substring(0, strMethod.length()-2);
			
			strMethod += ";";
			System.out.println(strMethod);
		}
		System.out.println("}");
		
	}
	
	
}


