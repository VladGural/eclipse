package com.gmail.vladgural;

import java.lang.reflect.*;

public class Main {

	public static void main(String[] args){
		ClassTest clsTest = new ClassTest();
		
		Class<?> cls = clsTest.getClass();
		
		Method[] methods = cls.getDeclaredMethods();
		
		for(Method method : methods) {
			if(method.isAnnotationPresent(Test.class)) {
				Test test = method.getAnnotation(Test.class);
				int a = test.a();
				int b = test.b();
				try {
					method.invoke(clsTest, a, b);					
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

}
