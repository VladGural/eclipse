import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Output extends HttpServlet {

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=utf-8");
		PrintWriter out = resp.getWriter();
		
		try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch (ClassNotFoundException e){
            out.println("Cannot create class");
            return;
        }
		
		String pageContent = "";
		
		try {
			Connection dbh = DriverManager.getConnection("jdbc:mysql://localhost:3306/SalesDept",
							"root",
							"Cod3281291");
			Statement query = dbh.createStatement();
			ResultSet qresult = query.executeQuery("SELECT * FROM Products");
			
			while (qresult.next()) {
				pageContent +=
				"<tr><td>" + qresult.getString("description") + "</td>" +
				"<td>" + qresult.getString("details") + "</td>" +
				"<td>" + qresult.getBigDecimal("price") + "</td></tr>";
			}
			
			
			out.println("<html>");
			out.println("<head>");
			out.println("<title>������ � MySQL</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<!� ������� ��������� ������ ������� �>");
			out.println("<h1>������ �������</h1>");
			out.println("<table>");
			out.println("<tr>");
			out.println("<th>������������</th>");
			out.println("<th>��������</th>");
			out.println("<th>����</th>");
			out.println("</tr>");
			out.println(pageContent);
			out.println("</table>");
			out.println("</body>");
			out.println("</html>");
			
			
		}catch(SQLException e) {
			out.println(e);
			out.println("������ ������� � ���� ������");
			return;
		}
	}
	

}
