import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ColorGetServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String color = req.getParameter("color");
		resp.setContentType("text/html");
		PrintWriter pw = resp.getWriter();
		pw.println("<b>The selected color is</b>");
		pw.println(color);
		pw.close();
	}
	
}
