import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddCookieServlet extends HttpServlet{

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String data = req.getParameter("data");
		Cookie cookie = new Cookie("MyCookie",data);
		resp.addCookie(cookie);
		
		resp.setContentType("text/html");
		PrintWriter pw = resp.getWriter();
		pw.println("<b>MyCocie had been set to");
		pw.println(data);
		pw.close();
	}
	

}

