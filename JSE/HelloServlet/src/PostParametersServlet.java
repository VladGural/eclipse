import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class PostParametersServlet extends GenericServlet {

	public void service(ServletRequest req, ServletResponse res) 
			throws ServletException, IOException {
		PrintWriter pw = res.getWriter();
		
		Enumeration<String> e = req.getParameterNames();
		
		while(e.hasMoreElements()) {
			String parameterName = (String) e.nextElement();
			pw.print(parameterName + " = ");
			String parameterValue = req.getParameter(parameterName);
			pw.println(parameterValue);
		}
		pw.close();
	}
	
	
}
