import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class UnitTestJ {
	private Unit unit;
	
	@Before
	public void setUp() {
		unit = new Unit();
	}
	
	@Test
	public void testSumSucces() {
				
		int actual = unit.sum(2, 2);
		int expected = 4;
		
		
		Assert.assertEquals(expected, actual);
		
		
	}
	
}
