import java.lang.reflect.Method;

public class Tester {
	public static void test(Class<?>... ls) {
		
		
		try {
			for (Class<?> cls : ls) {
				Method methodBefore = null;
				Method[] methods = cls.getDeclaredMethods();
				for (Method method : methods) {
					if (method.isAnnotationPresent(Before.class)) {
						methodBefore = method;
					}
				}
				
				for (Method method : methods) {
					if (method.isAnnotationPresent(Test.class)) {
						if(methodBefore!=null) 
							methodBefore.invoke(null, new Object[] {});
						method.invoke(null, new Object[] {});
						
					}
				}
			}
			
			//return true;
			
		} catch (Exception ex) {
			ex.printStackTrace();
			//return false;
		}
	}

}

