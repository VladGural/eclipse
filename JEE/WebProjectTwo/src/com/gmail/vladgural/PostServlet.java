package com.gmail.vladgural;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PostServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String address = req.getParameter("paramA");
		String phone = req.getParameter("paramB");
		PrintWriter pw = resp.getWriter();
		pw.println(address +" -> "+phone);
		pw.close();
	}
}
