package com.gmail.vladgural;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PostServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String firstnumber = req.getParameter("firstnumber");
		String secondnumber = req.getParameter("secondnumber");
		String operation = req.getParameter("operation");
		
		double val1, val2, res;
		String result="";
		
		try {
			val1=Double.valueOf(firstnumber);
			val2=Double.valueOf(secondnumber);
			switch(operation) {
				case "+":
					res=val1+val2;
					result=firstnumber + " + " + secondnumber + " = " + res;
					break;
				case "-":	
					res=val1-val2;
					result=firstnumber + " - " + secondnumber + " = " + res;
					break;
				case "*":	
					res=val1*val2;
					result=firstnumber + " * " + secondnumber + " = " + res;
					break;
				case "/":	
					res=val1/val2;
					result=firstnumber + " / " + secondnumber + " = " + res;
					break;
			}
			
		}catch(NumberFormatException e) {
			result="You entered illegal data";
		}
				
		PrintWriter pw = resp.getWriter();
		pw.println(result);
		pw.close();
	}
	
	

}
