package com.gmail.vladgural;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MainServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		String text = "<!DOCTYPE html>"
				+"<html>"
				+"<head>"
				+"	<meta charset=\"utf-8\">"
				+"	<title>Calculator</title>"
				+"</head>"
				+"<body>"
				+"	<center>"
				+"	<form name=\"Form\" method=\"post\" action=\"send\">"
				+"		<table>"
				+"			<tr>"
				+"				<td colspan=\"2\" align=\"center\"><p><b>Calculator</b></p></td>"
				+"			</tr>"
				+"			<tr>"
				+"				<td><p>First number</p></td>"
				+"				<td><input type=\"text\" name=\"firstnumber\" size=\"15\"></td>"
				+"			</tr>"
				+"			<tr>"
				+"				<td><p>Operation</p></td>"
				+"				<td>"
				+"					<label>+"
				+"						<input type=\"radio\" name=\"operation\" value=\"+\" checked=\"checked\">"						
				+"					</label>"
				+"					<label>-"
				+"						<input type=\"radio\" name=\"operation\" value=\"-\">"
				+"					</label>"
				+"					<label>*"
				+"						<input type=\"radio\" name=\"operation\" value=\"*\">"
				+"					</label>"
				+"					<label>/"
				+"						<input type=\"radio\" name=\"operation\" value=\"/\">"
				+"					</label>"
				+"				</td>"
				+"			</tr>"
				+"			<tr>"
				+"				<td><p>Second number</p></td>"
				+"				<td><input type=\"text\" name=\"secondnumber\" size=\"15\"></td>"
				+"			</tr>"
				+"			<tr>"
				+"				<td colspan=\"2\" align=\"center\">"
				+"					<input type=\"submit\" name=\"submit\" value=\"Get result\">"
				+"				</td>"
				+"			</tr>"
				+"		</table>"
				+"	</form>"
				+"	</center>"
				+"</body>"
				+"</html>";
		PrintWriter pw = arg1.getWriter();
		pw.println(text);
		pw.close();
		
		
	}
	
	
}
